var axios = require('axios');

const bearerID: string = "b0084ba396b1821256dbe44de774889d";

// Upload the files into YouSign server.
function setUpload(name: string, content: string, body: any) {
    var dataUpload = JSON.stringify({
        "name": name,
        "content": content
    });

    var configToUpload = {
    method: 'post',
    url: 'https://staging-api.yousign.com/files',
    headers: { 
        'Authorization': `Bearer ${bearerID}`, 
        'Content-Type': 'application/json'
    },
    data : dataUpload
    };

    // Start the process to upload the document
    axios(configToUpload)
    .then(function (res: any) {
        // Start the process to sign the document
        setProcess(res.data.id, body);
    })
    .catch(function (err: any) {
      console.log(err);
    });
}

// Start YouSign process for send the pdf to the differents users.
function setProcess(ID:string, body: any) {
    // Set users data
    var dataProcedure = JSON.stringify({
        "name": "My first procedure",
        "description": "Awesome! Here is the description of my first procedure",
        "members": [{
            "firstname": body.firstName1,
            "lastname": body.lastName1,
            "email": body.email1,
            "phone": body.phone1,
            "fileObjects": [{
                "file": ID,
                "page": 2,
                "position": "230,499,464,589",
                "mention": "Lu et approuvé",
                "mention2": "Signé par " + body.firstName1
            }]
        },
        {
            "firstname": body.firstName2,
            "lastname": body.lastName2,
            "email": body.email2,
            "phone": body.phone2,
            "fileObjects": [{
                "file": ID,
                "page": 2,
                "position": "230,499,464,589",
                "mention": "Lu et approuvé",
                "mention2": "Signé par " + body.firstName2
            }]
        }]
  });

  // Axios config
  var configToProcess = {
    method: 'post',
    url: 'https://staging-api.yousign.com/procedures',
    headers: { 
      'Authorization': `Bearer ${bearerID}`, 
      'Content-Type': 'application/json'
    },
    data : dataProcedure
  };

  // POST axios request
  axios(configToProcess)
  .then((res: any) => {
    console.log(res.data);
    console.log("DONE !");
  })
  .catch((err: any) => {
    console.log(err);
  })
}

module.exports = { setUpload };