const Actions = require("./src/actions");
const express = require('express');
const cors = require('cors');
const multer = require('multer');
const fs = require('fs');
const pdfParse = require('pdf-parse')
const app = express();
const port = 3000;

const moment = Date.now();

const storage = multer.diskStorage({
  destination: function (req: any, file: any, cb: Function) {
    cb(null, "dist/uploads");
  },
  filename: function (req: any, file: any, cb: Function) {
    //Creation du dossier uploads si inexistant
    if (!fs.existsSync(__dirname + "/uploads/")){
      fs.mkdirSync(__dirname + "/uploads/", { recursive: true });
    }
    //renommage du fichier à upload
    cb(null, moment + `_${file.originalname}`);
  },
});

var upload = multer({ storage: storage });

app.use(cors());

app.post('/uploads', upload.array("files"), (req: any, res: any) => {
  // For each files uploaded
  req.files.forEach((element: any) => {
    let fileSync = fs.readFileSync(__dirname + "/uploads/" + moment + '_' + element.originalname);
    try {
      pdfParse(fileSync)
      .then((r: any) => {
        Actions.setUpload(element.originalname, r.text);
      })
    } catch (e: any) {
      throw new Error(e)
    }
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})